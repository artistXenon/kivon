const isReadableStream = streamObject => 
    streamObject !== null &&
	typeof streamObject === 'object' &&
	typeof streamObject.pipe === 'function' &&
	streamObject.readable !== false &&
	typeof streamObject._read === 'function' &&
	typeof streamObject._readableState === 'object';





module.exports = {
    isReadableStream
}