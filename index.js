const path = require(`path`);
const { isValidTemplate, isValidMatcher } = require(`./templateParser`);

const ERR_NOT_JSON = `argument is not a type of JSON object nor string of JSON.`,
	ERR_INVALID_TEMPLATE = `given template is not in valid format`;

const toJSON = object => {
	if(object instanceof JSON) return object;
	if(typeof object === `string`) {
		try {
        		const result = JSON.parse(str);
		        const type = Object.prototype.toString.call(result);
			if(type === '[object Object]' || type === '[object Array]') 
				return result;
			else
				throw new Error();

    		} catch (err) {
	        throw new Error();
    		}
	}
	throw new Error();

}


const init = (configuration, template) => {
	if(global.kivon) 
		throw new Error(`Kivon has already initiated`);
	global.kivon = configuration;
		/*
	//check for invalid type
	const templateJSON = toJSON(template);
	if(!isValidTemplate(templateJSON))
		throw new Error();
	const configJSON = toJSON(configuration);


	return true;
*/
}

const update = (address, value) => {
	if(!global.kivon)
		throw new Error(`Kivon has not been initiated.`);
	throw new Error(`unsupported`);
		/*
	if(Object.prototype.toString.call(address) !== `[object Array]` || address.length === 0)
		throw new Error();
	let configurationAddress = global.kivon, templateAddress = global.kivonTemplate;
	address.forEach((key, index) => {
		if(typeof key !== `string`) 
			throw new Error();

		if(index + 1 === address.length) {
			if(true
			// value matches template matcher 
			) {
				configurationAddress[key] = value;
			}
			else throw new Error();
		}
		else {
			const tplAdrType = Object.prototype.toString.call(templateAddress[key]);
			const cfgAdrType = Object.prototype.toString.call(configurationAddress[key]);
			if(tplAdrType !== cfgAdrType) 
				throw new Error();
			if(tplAdrType !== `[object Array]` && tplAdrType !== `[object Object]`)
				throw new Error();
			//check if both cfg&tpl are Objects.
			//throw or continue
			
			templateAddress = templateAddress[key];
			configurationAddress = configurationAddress[key];

		}
	});
	*/
}

module.exports = {
	init,
	update
}
