const { isReadableStream } = require(`../util/stream`);

const CONSTANTS = require(`./commonConstants`);

const formatConstant2Parser = formatC => {
    switch(formatC) {
            case CONSTANTS.FORMAT_JSON:
                return require(`./JSON`);
            default:
                throw new Error(`INVALID FORMAT`);
        }

}


const parse = (objectFrom, formatFrom, formatTo) => {
    if(!isReadableStream(objectFrom))
        throw new Error(`INVALID ARGUMENT.\nThis is not user-reachable error. Please contact the kivon developer.`);
    if(formatTo === undefined)
        formatTo = CONSTANTS.FORMAT_JSON;
    let parseFrom, parseTo;
    let streamOut = objectFrom;
    if(formatFrom !== formatTo) {
        parseFrom = formatConstant2Parser(formatFrom);
        parseTo = formatConstant2Parser(formatTo);
        
        if(parseFrom.recursionLevel > parseTo.recursionLevel)
            throw new Error(`INCOMPATIBLE FORMAT CONVERSION`);
        streamOut = streamOut.pipe(parseFrom).pipe(parseTo);
    }

    return streamOut;    
};

module.exports = {
    //FUNCTIONS
    parse
}