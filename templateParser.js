const isValidMatcher = matcher => {

}

const isValidTemplate = tpl =>{
	const type = Object.prototype.toString.call(tpl);
	if(type === `[object Object]`) 
		return Object.values(tpl).every(isValidTemplate);
	else if(type === `[object Array]`) 
		return tpl.every(isValidTemplate);
	else if(type === `[object String]`) 
		return isValidMatcher(tpl);	
	else 
		return false;
}

const isValueMatch = (matcher, value) => {

}

const isConfigValid = (config, template) => {

}



module.exports = {
	isValidMatcher,
	isValidTemplate,
	isValueMatch,
	isConfigValid
};
